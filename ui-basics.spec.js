import {test, expect} from "@playwright/test"

test('open the browser and verify title', async({browser})=> {
    const newOne =  await browser.newContext();
    const page =  await newOne.newPage();
    await page.goto("https://www.google.com");
});

test.only('by using built-in properties', async({page}) => {
    const username = "rahulshetty";
    const password = "Ghaja@321";
    await page.locator('#username').fill(email);
    await page.locator('input[type="password"]').fill(password);
    await page.locator('input[name="signin"]').click();
   console.log(page.locator('.alert.alert-danger').textContent());
    await expect(page.locator('.alert.alert-danger')).toContainText('Incorrect');
});

test('Client App login', async ({ page }) => {
    const email = "akenya@gmail.com";
    const password = "Ghaja@321";
    const expectedTitles = [ 'ZARA COAT 3', 'ADIDAS ORIGINAL', 'IPHONE 13 PRO' ];
    await page.goto("https://rahulshettyacademy.com/client");
    await page.locator("#userEmail").fill(email);
    await page.locator("#userPassword").type(password);
    await page.locator("[value='Login']").click();
    await page.waitForLoadState('networkidle');
    await page.locator(".card-body b").first().textContent();
    console.log(await page.locator(".card-body b").first().textContent());
    await page.locator(".card-body b").first().waitFor();
    const titles = await page.locator(".card-body b").allTextContents();
    console.log(titles);
    expect(titles).toEqual(expectedTitles);
    await page.locator('.fa-sign-out').click();
    // await page.pause();
 })

 test("Handling the static dropdown", async({page}) => {
    const email = "rahulshetty";
    const password = "Ghaja@321";
    // const expectedTitles = [ 'ZARA COAT 3', 'ADIDAS ORIGINAL', 'IPHONE 13 PRO' ];
    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    await page.locator('#username').type(email);
    await page.locator('input[type="password"]').fill(password);
    await page.locator('select.form-control').selectOption('Teacher');
    await page.locator('#usertype').nth(1).click();
    await page.locator('#okayBtn').click();
    await expect(page.locator('#usertype').nth(1)).toBeChecked();
    const value = await page.locator('#usertype').nth(1).isChecked();
    expect(value).toBeTruthy();
    await expect(page.locator('[href*="documents-request"]')).toHaveAttribute("class", 'blinkingText');
    await page.pause();
 });

 test("handling child windows", async({browser}) => {
    const newOne =  await browser.newContext();
    const page =  await newOne.newPage();
    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    const documentLink = page.locator('[href*="documents-request"]');
    const [newPage] = await Promise.all([
        newOne.waitForEvent('page'),
        documentLink.click(),
    ]);
    const text = await newPage.locator('.red').textContent();
    console.log('text' + text);
    const splittedText = text.split("@")
    let username = splittedText[1].split(" ")[0];
    await page.locator('#username').type(username);
    await page.locator('input[type="password"]').fill('learning');
    await page.pause();
 })

 test('Client App end to end scenario', async ({ page }) => {
    const email = "akenya@gmail.com";
    const password = "Ghaja@321";
    const productName = "IPHONE 13 PRO";
    const productLocator = page.locator('div.card-body');
    await page.goto("https://rahulshettyacademy.com/client");
    await page.locator("#userEmail").fill(email);
    await page.locator("#userPassword").type(password);
    await page.locator("[value='Login']").click();
    await page.locator(".card-body b").first().waitFor();
    const titles = await page.locator('div.card-body b').allTextContents();
    console.log('no of products' +titles);
    const count = await productLocator.count();
    console.log('no of products' +count);
    for(let i=0; i<count; ++i){
        if(await productLocator.nth(i).locator("b").textContent() === productName){
            await productLocator.nth(i).locator("text=Add To Cart").click();
            break;
        }
    }
    const cartSection =  page.locator('div.infoWrap');
    await page.locator('[routerlink="/dashboard/cart"]').click();
    await expect(cartSection.locator('div h3')).toHaveText(productName);
    console.log(await cartSection.locator('div h3').textContent());
    cartSection.locator('div h3').first().waitFor();
    const bool = cartSection.locator('div h3').isVisible();
    // h3:has-text=''
    expect(bool).toBeTruthy();
    await page.locator("text=Checkout").click();
    await page.locator("[placeholder*='Country']").type("ind",{delay:100});
   const dropdown = page.locator(".ta-results");
   await dropdown.waitFor();
   const optionsCount = await dropdown.locator("button").count();
   for (let i = 0; i < optionsCount; ++i) {
      const text = await dropdown.locator("button").nth(i).textContent();
      if (text === " India") {
         await dropdown.locator("button").nth(i).click();
         break;
      }
   }
 
   await expect(page.locator(".user__name [type='text']").first()).toHaveText(email);
   await page.locator(".action__submit").click();
   await expect(page.locator(".hero-primary")).toHaveText(" Thankyou for the order. ");
 });